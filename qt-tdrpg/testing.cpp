#include <iostream>

#include "board.hpp"
#include "occupant.hpp"
#include "tile.hpp"

int main(){
    std::cout << "INTIALISING TEST" << std::endl;
    TDRPG::Board *x = new TDRPG::Board(10);
    x->generateBoard();
    x->cleanBoard();
    delete x;
}