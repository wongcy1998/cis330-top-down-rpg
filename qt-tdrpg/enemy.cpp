/*
 *	enemy.cpp
 *		Author: SegFaultLyfe (Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include "enemy.hpp"

namespace TDRPG {
	Enemy::Enemy() {
		type = OccupantType::ENEMY;
		HP = 5;
		ATK = 7;
		DEF = 2;
	}

    Enemy::Enemy(int hp, int atk, int def) {
        HP = hp;
        ATK = atk;
        DEF = def;
    }

	Enemy::Enemy(bool isboss) {
		if (isboss) {
			type = OccupantType::BOSS;
			// TODO: Determine good HP, ATK, and DEF for boss
			HP = 50;
			ATK = 10;
			DEF = 10;
		}
		else {
			Enemy();
		}
	}
}
