/*
 *	enemy.hpp
 *		Author: SegFaultLyfe (Joseph Goh, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_

#include "occupant.hpp"

namespace TDRPG {
	class Enemy : public Occupant {
	public:
		Enemy();
        Enemy(int hp, int atk, int def);
		Enemy(bool isboss);
	};
}

#endif // !ENEMY_HPP_
