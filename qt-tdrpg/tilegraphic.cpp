//Created by Jake Gianola
#include "tilegraphic.hpp"

namespace TDRPG {
    //Default constructor
    Tilegraphic::Tilegraphic(QWidget *parent) : QLabel (parent) {
        setPixmap((QPixmap(":/images/Resources/bettertile.png")));
    }

    //Copy constructor
    Tilegraphic::Tilegraphic(Tilegraphic& tile){
        setPixmap((QPixmap(":/images/Resources/bettertile.png")));
    }

    //Update Graphics
    void Tilegraphic::update(Tile tile){
        if (tile.isWalled()) {
            setPixmap((QPixmap(":/images/Resources/wall.png")));
        }
        else if (tile.isOccupied()){
            Occupant occupant = tile.getOccupant();

            if (occupant.getType() == OccupantType::ENEMY){
                setPixmap((QPixmap(":/images/Resources/enemy.png")));
            }
            else if (occupant.getType() == OccupantType::BOSS){
                setPixmap((QPixmap(":/images/Resources/boss.png")));
            }
            else if (occupant.getType() == OccupantType::PLAYER){
                setPixmap((QPixmap(":/images/Resources/player.png")));
            }
        }
        else {
            setPixmap((QPixmap(":/images/Resources/bettertile.png")));
        }
        repaint();
    }
}
