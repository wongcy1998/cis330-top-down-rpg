/*
 *	playercontroller.cpp
 *		Author: SegFaultLyfe (Joseph Goh, Jake Gianola, *add your name!*)
 */

#include "playercontroller.hpp"

namespace TDRPG {
    PlayerController::PlayerController(Player plyr) {
        this->player = plyr;
    }

    void PlayerController::interact(Board brd, int dx, int dy) {
        if (cur_x + dx >= 0 && cur_x + dx < brd.getSize() && cur_y + dy >= 0 && cur_y + dy < brd.getSize()){
            Tile target = brd.getTile(cur_x + dx, cur_y + dy);

            // If the player encounters an enemy then battle it
            if (target.isOccupied()) {
                Occupant enemy = target.getOccupant();
                if (enemy.getDEF() - player.getATK() < 0) {
                    enemy.setHP(enemy.getHP() + enemy.getDEF() - player.getATK());
                }
                if (player.getDEF() - enemy.getATK() < 0) {
                    player.setHP(player.getHP() + player.getDEF() - enemy.getATK());
                }
                // If enemy is defeated, move to that tile, and heal and upgrade player even if his HP went to zero.
                if (enemy.getHP() <= 0) {
                    score += enemy.getATK() + enemy.getDEF();
                    target.vacate();
                    move(brd, dx, dy);
                    // Heal 6 HP upon defeating an enemy
                    player.setHP(player.getHP() + 6);
                    // Upgrade ATK by 1
                    player.setATK(player.getATK() + 1);
                }
                else {
                    player.die();
                }
            }
            // If the target tile is not occupied just move
            else {
                move(brd, dx, dy);
            }
        }
        else {
            return;
        }
    }

    bool PlayerController::move(Board brd, int dx, int dy) {
        /*
        bool success;
        success = brd.swapTiles(cur_x, cur_y, cur_x + dx, cur_y + dy);
        if (success) {
            cur_x += dx;
            cur_y += dy;
        }
        return success;
        */
        brd.getTile(cur_x, cur_y).vacate();
        brd.getTile(cur_x + dx, cur_y + dy).setOccupant(player);
        return true;
    }

    int PlayerController::getX() {
        return cur_x;
    }

    int PlayerController::getY() {
        return cur_y;
    }

    void PlayerController::setX(int new_x) {
        cur_x = new_x;
    }

    void PlayerController::setY(int new_y) {
        cur_y = new_y;
    }
}
