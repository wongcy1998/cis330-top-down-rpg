/*
 *	occupant.hpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#ifndef OCCUPANT_HPP_
#define OCCUPANT_HPP_

namespace TDRPG {
    enum class OccupantType {EMPTY, PLAYER, ENEMY, BOSS, OBJECT};
    // Abstract base class for enemies, players, immovable objects, etc.
    class Occupant {
    public:
        // Setter methods
        void setType(OccupantType X);
        void setHP(int new_hp);
        void setATK(int new_atk);
        void setDEF(int new_def);
        void die();

        // Getter methods
		OccupantType getType();
		int getHP();
		int getATK();
		int getDEF();

        // Combat functions
        void damage(int amt);
    protected:
        OccupantType type;
        int HP;
        int ATK;
        int DEF;
    };
}


#endif // !OCCUPANT_HPP_
