/*
 *	tile.cpp
 *		Author: SegFaultLyfe (Joseph Goh, Toby Wong, ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#include <stdexcept>

#include "tile.hpp"

namespace TDRPG{
    Tile::Tile(){}

    Occupant Tile::getOccupant() {
		return cur_occupant;
    }

    OccupantType Tile::getSpawnType(){
					return spawn_type;
		}

		bool Tile::isTouched(){
			return touched;
		}

		bool Tile::isWalled() {
			return walled;
		}

		bool Tile::isOccupied() {
			return occupied;
		}

		void Tile::setOccupant(Occupant occup) {
			cur_occupant = occup;
			occupied = true;
		}

		void Tile::setSpawnType(OccupantType type){
			spawn_type = type;
		}

		void Tile::setTouched(bool status){
			touched = status;
		}

		void Tile::setWalled(bool status) {
			walled = status;
		}

		void Tile::vacate() {
			occupied = false;
		}
}
