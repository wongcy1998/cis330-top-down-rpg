/*
 *	tile.hpp
 *		Author: SegFaultLyfe (Joseph Goh, Jake Gianola, Toby Wong ADD YOUR NAME IF YOU WORK ON IT HERE)
 */

#ifndef TILE_HPP_
#define TILE_HPP_

#include "occupant.hpp"

namespace TDRPG {

    class Tile/*: public QObject, public QGraphicsPixmapItem*/ {
        // Q_OBJECT
    public:
        // Constructors
        Tile();

        // Get Occupant Object
        Occupant getOccupant();
        // Get initial spawn Type Info
        OccupantType getSpawnType();
        // Get Touched Status
        bool isTouched();
        // Get Walled Status
        bool isWalled();
        // Get Occupied Status
        bool isOccupied();

        // Setter for Occupant
        void setOccupant(Occupant occup);
        // Setter for Type of Occupant to be spawned
        void setSpawnType(OccupantType type);
        // Setter for if Tile touched
        void setTouched(bool status);
        // Setter for Walled Status
        void setWalled(bool status);
		
		// Vacate the tile (does not kill or call the Occupant deconstructor for now)
		void vacate();

    private:
        bool touched = false;
        bool walled = false;
        bool occupied = false;
        OccupantType spawn_type;
		Occupant cur_occupant;
    };
}

#endif // !TILE_HPP_

